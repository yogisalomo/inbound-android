package com.inbound;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main) ;

		Button btnLeaderboard = (Button) findViewById(R.id.leaderboard_btn);
		Button btnReport = (Button) findViewById(R.id.lapor_btn);
		// Set OnClick Listener on Report button
		btnReport.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intentUpload = new Intent(getApplicationContext(),
						UploadActivity.class);
				startActivity(intentUpload);
			}
		});
		// Set OnClick Listener on Leaderboard button
		btnLeaderboard.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intentLeaderboard = new Intent(getApplicationContext(),
						LeaderboardActivity.class);
				startActivity(intentLeaderboard);
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}