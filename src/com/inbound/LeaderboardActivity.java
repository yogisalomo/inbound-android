package com.inbound;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class LeaderboardActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.leaderboard);
		Button btnBack = (Button) findViewById(R.id.back_btn);
		// Set OnClick Listener on Report button
		btnBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated metshod stub

				Intent intentMain = new Intent(getApplicationContext(),
						MainActivity.class);
				startActivity(intentMain);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.leaderboard, menu);
		return true;
	}

}
