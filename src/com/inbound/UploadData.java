package com.inbound;

public class UploadData {
	String sourceFileUri;
	double latitude;
	double longitude;
	double facebook_id;
	int category_id;
	String report;
	
	public UploadData() {
		// TODO Auto-generated constructor stub
		sourceFileUri = "dummy";
		latitude = 0;
		longitude = 0;
		facebook_id = 0;
		category_id = 1;
		report = "dummy";
	}
	
	public UploadData(String sourceFile, double lat, double longi, double fId, int catID, String rep){
		sourceFileUri = sourceFile;
		latitude = lat;
		longitude = longi;
		facebook_id = fId;
		category_id = catID;
		report = rep;
	}
	
	public double getFacebook_id() {
		return facebook_id;
	}
	public double getLatitude() {
		return latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public String getReport() {
		return report;
	}
	public String getSourceFileUri() {
		return sourceFileUri;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setFacebook_id(double facebook_id) {
		this.facebook_id = facebook_id;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public void setSourceFileUri(String sourceFileUri) {
		this.sourceFileUri = sourceFileUri;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
}
