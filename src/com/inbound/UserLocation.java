package com.inbound;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

public class UserLocation extends android.app.Service implements LocationListener {
	private final Context mContext;

	boolean isGPSOn = false;
	boolean isNetwrokOn = false;
	boolean canGetLocation = false;

	Location location;
	double latitude;
	double longitude;

	private static final float DELTA_DISTANCE = 10; // 10 meter
	private static final long DELTA_TIME = 1000 * 60; // 1 menit

	protected LocationManager locationManager;
	
	public UserLocation(Context context) {
		// TODO Auto-generated constructor stub		
		this.mContext = context;
		getLocation();
	}

	public Location getLocation() {
		try {
			locationManager = (LocationManager) mContext
					.getSystemService(android.content.Context.LOCATION_SERVICE);

			// assign flag dengan kondisi nyata
			isGPSOn = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
			isNetwrokOn = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSOn && !isNetwrokOn) {
				// jika keduanya service mati
			} else {
				if (isGPSOn) {
					this.canGetLocation = true;
					// jika GPS nyala
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER, DELTA_TIME,
								DELTA_DISTANCE, this);
					}

					Log.d("GPS ENABLED", "GPS ENABLED");
					if (locationManager != null) {
						location = locationManager
								.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						if (location != null) {
							latitude = location.getLatitude();
							longitude = location.getLongitude();
							System.out.println("LONGITUDE GPS: " + longitude);
							System.out.println("LATITUDE GPS: " + latitude);
						}
					}
				}

				if (isNetwrokOn) {
					// Jika Network nyala
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.NETWORK_PROVIDER, DELTA_TIME,
								DELTA_DISTANCE, this);
						Log.d("NETWORK ENABLED", "NETWORK ENABLED");
						if (locationManager == null) {
							location = locationManager
									.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
							if (location != null) {
								longitude = location.getLongitude();
								latitude = location.getLatitude();
								System.out.println("LONGITUDE NETWORK : "
										+ longitude);
								System.out.println("LATITUDE NETWORK : "
										+ latitude);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return location;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}
		return latitude;
	}

	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}
		return longitude;
	}

	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	public void showSettingsAlert() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog
				.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// Setting Icon to Dialog
		// alertDialog.setIcon(R.drawable.delete);

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						mContext.startActivity(intent);
					}
				});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}
	
	public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(UserLocation.this);
        }
        
        Log.i("GPS", "GPS Turn OFF");
    }
	

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
}
